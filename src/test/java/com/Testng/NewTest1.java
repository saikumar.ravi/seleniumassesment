package com.Testng;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class NewTest1  {
	WebDriver driver;


	@BeforeMethod
	private WebDriver launchbrowser() {
		if ( driver==null) 
		{
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			driver.get("https://admin-demo.nopcommerce.com/login");
		}
		return driver;
		}
	@Test(priority = 1)
	private void logInApplication() {
		driver.findElement(By.id("Email")).clear();
		driver.findElement(By.id("Email")).sendKeys("admin@yourstore.com");
		driver.findElement(By.id("Password")).clear();
		driver.findElement(By.id("Password")).sendKeys("admin");
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		String Actualname="John Smith";
		String ExpectedName=driver.findElement(By.linkText("John Smith")).getText();
		System.out.println(ExpectedName);
		Assert.assertEquals(Actualname, ExpectedName);}
	@Test(priority = 2)

	private void categories() throws IOException {
		// public void
		File f=new File("/home/Saikumar/Documents/Test_data/seleniumdata1.xlsx");
		FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook b=new XSSFWorkbook(fis);
		XSSFSheet sheet=b.getSheetAt(0);
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<=rows;i++) {
			String categoryname=sheet.getRow(i).getCell(0).getStringCellValue();
			String searchbox=sheet.getRow(i).getCell(1).getStringCellValue();
		driver.findElement(By.xpath("(//a/p[contains(text(), 'Catalog')])[1]")).click();
		WebElement dropdown1=driver.findElement(By.xpath("(//a/p[contains(text(), 'Categories')])"));

		dropdown1.click();
		driver.findElement(By.xpath("//a[@href='/Admin/Category/Create']")).click();
		driver.findElement(By.id("Name")).sendKeys(categoryname);
		WebElement dropdown3=driver.findElement(By.id("ParentCategoryId"));
		Select s=new Select(dropdown3);
		s.selectByVisibleText("Computers >> Build your own computer");
		driver.findElement(By.name("save")).click();
		driver.findElement(By.id("SearchCategoryName")).sendKeys(searchbox);
		driver.findElement(By.id("search-categories")).click();
		}
		//String ActualProductname="Computers >> Build your own computer >> Dell and Lenova";
		// String ExpectedProductName=driver.findElement(By.partialLinkText("Computers >> Build your own computer >> Dell and Lenova")).getText();
		//System.out.println(ExpectedProductName);
		//Assert.assertEquals(ActualProductname, ExpectedProductName);
				String text = driver.findElement(By.xpath("//td[contains(text(),'Dell and Lenovo')]")).getText();
				Assert.assertTrue(text.contains("Dell and Lenovo"));}
	
	@Test(priority = 3)
	private void products() {
		

	driver.findElement(By.xpath("(//a/p[contains(text(), 'Catalog')])[1]")).click();
	driver.findElement(By.xpath("(//a/p[contains(text(), 'Products')])")).click();
	driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
	WebElement dropdown4=driver.findElement(By.id("SearchCategoryId"));
	Select s1=new Select(dropdown4);
	s1.selectByVisibleText("Computers>>Desktops");
	driver.findElement(By.id("search-products")).click();
	String ActualProductname1="Build your own computer";
	String ExpectedProductName1=driver.findElement(By.xpath(" //td[contains(text(),'Build your own computer')]")).getText();
	System.out.println(ExpectedProductName1);

	Assert.assertTrue(ExpectedProductName1.contains(ActualProductname1));
}
@Test(priority = 4)
private void manufacturers() {
	driver.findElement(By.xpath("(//a/p[contains(text(), 'Catalog')])[1]")).click();
	driver.findElement(By.xpath("(//a/p[contains(text(), 'Manufacturers')])")).click();
	driver.findElement(By.xpath("//a[@href='/Admin/Manufacturer/Create']")).click();
	driver.findElement(By.id("Name")).sendKeys("RedmiNotebook");
	driver.findElement(By.name("save")).click();
	String ActualProductname2="RedmiNotebook";
	String ExpectedProductName2=driver.findElement(By.xpath("//td[contains(text(),'RedmiNotebook')]")).getText();
	System.out.println(ExpectedProductName2);

	Assert.assertTrue(ExpectedProductName2.contains(ActualProductname2));
}
@Test(priority = 5)
private void logOut() {
	driver.findElement(By.linkText("Logout")).click();
}








}

